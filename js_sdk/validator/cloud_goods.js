// 表单校验规则由 schema2code 生成，不建议直接修改校验规则，而建议通过 schema2code 生成, 详情: https://uniapp.dcloud.net.cn/uniCloud/schema



const validator = {
	"id": {
		"rules": [{
			"format": "int"
		}],
		"label": "编号"
	},
	"src": {
		"rules": [{
			"format": "string"
		}],
		"label": "封面图"
	},
	"banner": {
		"rules": [{
			"format": "string"
		}],
		"label": "横图"
	},
	"title": {
		"rules": [{
				"required": true
			},
			{
				"format": "string"
			}
		],
		"defaultValue": "",
		"label": "商品名称"
	},
	"subTitle": {
		"rules": [{
			"format": "string"
		}],
		"defaultValue": "",
		"label": "子标题"
	},
	"price": {
		"rules": [{
				"required": true
			},
			{
				"format": "double"
			}
		],
		"defaultValue": 0,
		"label": "销售价格"
	},
	"originPrice": {
		"rules": [{
			"format": "double"
		}],
		"defaultValue": 0,
		"label": "原价"
	},
	"monthlySale": {
		"rules": [{
			"format": "int"
		}],
		"defaultValue": 0,
		"label": "月售量"
	},
	"categories": {
		"rules": [{
			"format": "array"
		}],
		"label": "分类"
	},
	"shopid": {
		"rules": [{
				"required": true
			},
			{
				"format": "int"
			}
		],
		"label": "店铺"
	},
	"stock": {
		"rules": [{
				"required": true
			},
			{
				"format": "int"
			}
		],
		"defaultValue": 100,
		"label": "剩余库存"
	},
	"visite": {
		"rules": [{
			"format": "int"
		}],
		"defaultValue": 0,
		"label": "浏览量"
	},
	"limit": {
		"rules": [{
			"format": "int"
		}],
		"defaultValue": 0,
		"label": "限购数量"
	},
	"isSold": {
		"rules": [{
				"format": "int"
			},
			{
				"range": [{
						"text": "下架",
						"value": 0
					},
					{
						"text": "在售",
						"value": 1
					}
				]
			}
		],
		"defaultValue": 0,
		"label": "是否销售"
	},
	"isLimit": {
		"rules": [{
				"format": "bool"
			},
			{
				"range": [{
						"text": "否",
						"value": false
					},
					{
						"text": "是",
						"value": true
					}
				]
			}
		],
		"defaultValue": false,
		"label": "是否限购"
	},
	"isNewUser": {
		"rules": [{
			"format": "bool"
		}],
		"defaultValue": false,
		"label": "是否新用户"
	},
	"isRecommend": {
		"rules": [{
				"format": "bool"
			},
			{
				"range": [{
						"text": "否",
						"value": false
					},
					{
						"text": "是",
						"value": true
					}
				]
			}
		],
		"defaultValue": false,
		"label": "是否推荐"
	},
	"posid": {
		"rules": [{
			"format": "int"
		}],
		"defaultValue": 0,
		"label": "排序"
	},
	"onlySelfTake": {
		"rules": [{
				"format": "int"
			},
			{
				"range": [{
						"text": "否",
						"value": 0
					},
					{
						"text": "是",
						"value": 1
					}
				]
			}
		],
		"defaultValue": 0,
		"label": "仅自提"
	},
	"saleType": {
		"rules": [{
				"format": "string"
			},
			{
				"range": [{
						"text": "正常销售",
						"value": "normal"
					},
					{
						"text": "提前预定",
						"value": "yuding"
					},
					{
						"text": "社区团购",
						"value": "tuangou"
					}
				]
			}
		],
		"label": "销售类型"
	},
	"score": {
		"rules": [{
			"format": "int"
		}],
		"defaultValue": 10,
		"label": "评分"
	},
	"share": {
		"rules": [{
			"format": "string"
		}],
		"label": "分享词"
	},
	"skus": {
		"rules": [{
			"format": "array"
		}],
		"label": "多规格"
	},
	"skuname": {
		"rules": [{
			"format": "array"
		}],
		"label": "规格名称"
	},
	"specsType": {
		"rules": [{
				"format": "string"
			},
			{
				"range": [{
						"text": "单规格",
						"value": "single"
					},
					{
						"text": "多规格",
						"value": "multiple"
					}
				]
			}
		],
		"label": "规格类型"
	},
	"storage_condition": {
		"rules": [{
			"format": "string"
		}],
		"label": "储存条件"
	},
	"net_weight": {
		"rules": [{
			"format": "string"
		}],
		"label": "净重"
	},
	"manufacture": {
		"rules": [{
			"format": "string"
		}],
		"label": "生成日期"
	},
	"guarantee_period": {
		"rules": [{
			"format": "string"
		}],
		"label": "保质期"
	},
	"tuangou": {
		"rules": [{
			"format": "object"
		}],
		"label": "团购信息"
	},
	"upc": {
		"rules": [{
			"format": "string"
		}],
		"label": "条码"
	},
	"imgs": {
		"rules": [{
			"format": "array"
		}],
		"label": "轮播图"
	}
}

const enumConverter = {
	"isSold_valuetotext": {
		"0": "下架",
		"1": "在售"
	},
	"isLimit_valuetotext": [{
			"text": "否",
			"value": false
		},
		{
			"text": "是",
			"value": true
		}
	],
	"isRecommend_valuetotext": [{
			"text": "否",
			"value": false
		},
		{
			"text": "是",
			"value": true
		}
	],
	"onlySelfTake_valuetotext": {
		"0": "否",
		"1": "是"
	},
	"saleType_valuetotext": [{
			"text": "正常销售",
			"value": "normal"
		},
		{
			"text": "提前预定",
			"value": "yuding"
		},
		{
			"text": "社区团购",
			"value": "tuangou"
		}
	],
	"specsType_valuetotext": [{
			"text": "单规格",
			"value": "single"
		},
		{
			"text": "多规格",
			"value": "multiple"
		}
	],
	"skunames": [
		'尺寸',
		'型号',
		'颜色',
		'类别',
		'重量',
		'款式',
		'货号',
		'器型',
		'材质',
		'尺码',
		'口味',
		'色号',
		'容量',
		'花型',
		'地点',
		'香型',
		'成分',
		'版本',
		'度数',
		'属性',
		'地区',
		'套餐',
		'功效',
		'品类',
		'时间',
		'组合',
		'运营商',
		'提货点',
		'适用年龄',
		'适用人群'
	]
}

export {
	validator,
	enumConverter
}
