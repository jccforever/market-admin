
// 表单校验规则由 schema2code 生成，不建议直接修改校验规则，而建议通过 schema2code 生成, 详情: https://uniapp.dcloud.net.cn/uniCloud/schema



const validator = {
  "content": {
    "rules": [
      {
        "format": "string"
      }
    ],
    "label": "内容"
  },
  "reply": {
    "rules": [
      {
        "format": "string"
      }
    ],
    "label": "回复"
  },
  "goods_id": {
    "rules": [
      {
        "format": "string"
      }
    ],
    "label": "商品"
  },
  "index": {
    "rules": [
      {
        "format": "int"
      }
    ],
    "label": "索引"
  },
  "order_id": {
    "rules": [
      {
        "format": "string"
      }
    ],
    "label": "订单id"
  },
  "shopid": {
    "rules": [
      {
        "format": "int"
      }
    ],
    "label": "店铺"
  },
  "star": {
    "rules": [
      {
        "format": "int"
      }
    ],
    "label": "评分"
  },
  "starResult": {
    "rules": [
      {
        "format": "string"
      }
    ],
    "label": "评分内容"
  },
  "tags": {
    "rules": [
      {
        "format": "array"
      }
    ],
    "label": "标签"
  },
  "user": {
    "rules": [
      {
        "format": "object"
      }
    ],
    "label": "用户信息"
  }
}

const enumConverter = {}

export { validator, enumConverter }
