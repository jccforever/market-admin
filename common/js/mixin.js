export default {
	methods: {
		/**
		 * 复制单条数据，_id重新生成
		 * @param {Object} id
		 * @param {Object} dbCollectionName
		 */
		copy(id, dbCollectionName) {
			uni.showLoading({
				title: "正在复制",
				mask: true
			})
			uniCloud.database().action("copy").collection(dbCollectionName).doc(id).get().then(res => {
				console.log(res)
				uni.hideLoading()
				uni.showToast({
					title: '复制成功'
				})
				setTimeout(() => {
					try {
						this.loadData(true);
					} catch (e) {}
				}, 1500)
			});
		},
		/**
		 * 切换switch组件，同步value
		 * @param {Object} name
		 * @param {Object} value
		 * @param {Object} formName
		 */
		changeSwich(name, value, formName) {
			if (this.formData) {
				this.formData[name] = value;
			} else {
				this[name] = value;
			}
			if (formName) {
				this.$refs[formName].setValue(name, value)
			} else {
				let formVm
				for (let i in this.$refs) {
					const vm = this.$refs[i]
					if (vm && vm.$options && vm.$options.name === 'uniForms') {
						formVm = vm
						break
					}
				}
				if (!formVm) return console.error('当前 uni-froms 组件缺少 ref 属性')
				formVm.setValue(name, value)
			}
		}
	}
}
